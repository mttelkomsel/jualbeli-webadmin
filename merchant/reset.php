<?php
require_once('config.php');
if(isset($_POST) & !empty($_POST)){
	$email = mysqli_real_escape_string($conn, $_POST['email']);
	$newpass=randomPassword();
	$password = md5($newpass);

	$sql = "Select `username` from `user_merchant` where email='".$email."'";
	$result = mysqli_query($conn, $sql);
	if($result){
		 while ($row=mysqli_fetch_row($result))
		 {
			 $userid=$row[0];
		 }
	}
	if($userid==""){
		$fmsg = "Reset password gagal, masukan email kamu dengan benar";
	}
	$sql = "UPDATE `user_merchant` set password='".$password."' where email='".$email."'";
	//echo $sql;
	//exit();
	$result = mysqli_query($conn, $sql);
	if($result){
		$smsg = "Reset password berhasil, silakan cek email kamu";
		// the message
		
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <Admin@market.mtt.or.id>' . "\r\n";
		
		$msg ="<html><body>";
		$msg .= "<h3>Dear Merchant Jual Beli,</h3>";
		$msg .="<br>";
		$msg .= "Terima kasih sudah menjadi merchant kami, berikut ini informasi akun kamu:";
		$msg .="<br>";
		$msg .= "<table rules=\"all\" style=\"border-color: #666;\" cellpadding=\"10\">";
		$msg .= "<tr style=\"background: #eee;\"><td><strong>Username:</strong> </td><td>" . $userid . "</td></tr>";
		$msg .= "<tr style=\"background: #eee;\"><td><strong>Password:</strong> </td><td>" . $newpass. "</td></tr>";
		$msg .= "</table>";
		$msg .="<br>";
		$msg .="<br>";
		$msg .="Salam,";
		$msg .="<br>";
		$msg .="Admin Jual Beli";
		$msg .="</body><html>";
		
		// use wordwrap() if lines are longer than 70 characters
		$msg = wordwrap($msg,70);

		// send email
		mail($email,"Jual Beli : Reset Password", $msg, $headers);

		header('location: reset.php?smsg=Reset password berhasil, silakan cek email kamu');

	}else{
		$fmsg = "Reset password gagal, masukan email kamu dengan benar";
	}
	//echo $sql;
	//exit();
}
function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
$smsg=$_REQUEST['smsg'];


?>
<!DOCTYPE html>
<html>
<head>
	<title>Password Reset Jual Beli</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css" >

	<!-- Latest compiled and minified JavaScript -->
	<script src="js/bootstrap.min.js" ></script>

	<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
      <?php if(isset($smsg)){ ?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php } ?>
      <?php if(isset($fmsg)){ ?><div class="alert alert-danger" role="alert"> <?php echo $fmsg; ?> </div><?php } ?>
      <form class="form-signin" method="POST">
        <h2 class="form-signin-heading">Reset Password</h2>
        <label for="inputEmail" class="sr-only">Email</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
        <button class="btn btn-lg btn-success btn-block" type="submit">Reset</button>
        <a class="btn btn-lg btn-success btn-block" href="login.php">Login</a>
      </form>
</div>
</body>
</html>