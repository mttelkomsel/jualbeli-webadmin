<?php
	include('lib/xcrud/xcrud.php');
	//Xcrud_config::$editor_url = dirname($_SERVER["SCRIPT_NAME"]).'/editors/tinymce/tinymce.min.js'; // can be set in config
	$xcrud = Xcrud::get_instance();

	$xcrud->theme('bootstrap');
    $xcrud->table('product');
	$xcrud->unset_print(true);
	$xcrud->unset_csv(true);
	
	//$xcrud->join('id','product_image','product_id'); 
	//$xcrud->query('SELECT * FROM product left join product_image on product.id=product_image.product_id 
	//where merchant_id='.$userid.' order by last_update desc');
    $xcrud->table_name('Product');
    $xcrud->columns('category_id,merchant_id,created_at,last_update', true);
	$xcrud->fields('merchant_id,created_at,last_update', true);
    $xcrud->change_type('status','select','READY STOCK','READY STOCK, OUT OF STOCK, SUSPEND');
	
	$xcrud->relation('category_id','category','id','name',array('draft' => 0),'priority asc',true);

    $xcrud->order_by('last_update','desc');
	$xcrud->where('merchant_id =', $userid);
	
	//$xcrud->hide_button('edit');
    $xcrud->unset_view();
    $xcrud->column_pattern('name', '<a href="#" class="xcrud-action" data-task="edit" data-primary="{id}">{name}</a>');
   
	$mynow=round(microtime(true)*1000);
	$xcrud->pass_var('created_at',$mynow);
	$xcrud->pass_var('last_update',$mynow);


	$xcrud->pass_var('merchant_id',$userid);
	//$xcrud->before_insert('update_product');
    //$xcrud->before_update('update_product');
    $xcrud->after_insert('insert_product');
    $xcrud->after_update('update_product');
	//$xcrud->after_update('update_category');

	// simple image upload
	//$xcrud->change_type('simple_image', 'image');

	// image upload with resizing
	$xcrud->change_type('image', 'image', '', array('not_rename'=>true,'width' => 200, 'height' => 200,'path'=>'/var/www/vhosts/mtt.or.id/market.mtt.or.id/panel/uploads/product/'));

//$xcrud->subselect('image2','SELECT name FROM product_image WHERE product_id = {id}'); // insert as last column

//$xcrud->change_type('image2', 'image', '', array('not_rename'=>true,'width' => 200, 'height' => 200,'path'=>'/var/www/vhosts/mtt.or.id/market.mtt.or.id/panel/uploads/product/'));

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>Produk Kamu</title>
</head>
 
<body>
 
<?php
    echo $xcrud->render();
?>
 
</body>
</html>