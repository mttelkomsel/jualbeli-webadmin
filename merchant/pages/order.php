<?php
// Turn off all error reporting
error_reporting(0);
//error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

    include('lib/xcrud/xcrud.php');
    
    $xcrud = Xcrud::get_instance();
	$xcrud->theme('bootstrap');	
    $xcrud->table('product_order_detail');
	$xcrud->unset_print(true);
	$xcrud->unset_csv(true);
	

	$xcrud->join('product_id','product','id'); 
	$xcrud->join('order_id','product_order','id'); 

    $xcrud->table_name('Order');
	
	$xcrud->columns('product_order.code,product_order_detail.product_name,product_order_detail.amount,product_order_detail.price_item,product_order.buyer,product_order.date_time,product_order.address,product_order.shipping,product_order.comment,product_order.phone, product_order.status, product_order.tax,product_order.total_fees',false);
	
	$myint='(int){product_order.created_at}';
    $time=(int)($myint/1000);

    $xcrud->order_by('last_update','desc');
	$xcrud->where('product.merchant_id =', $userid);

	$xcrud->unset_add();
	//$xcrud->unset_edit();
	$xcrud->unset_remove();
    $xcrud->unset_view();
    //$xcrud->column_pattern('name', '<a href="#" class="xcrud-action" data-task="edit" data-primary="{id}">{name}</a>');
   


?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>History Order</title>
</head>
 
<body>
 
<?php
    echo $xcrud->render();
?>
 
</body>
</html>