<?php
session_start();
error_reporting(E_ERROR | E_PARSE);
/*echo "aaaaaaa".$_SESSION['username'];
echo isset($_SESSION['username']);
exit();
*/
if(isset($_SESSION['username'])){
	$username=$_SESSION['username'];
	$userid=$_SESSION['userid'];
	$apps=$_SESSION['apps'];
}else{
  header('location: login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0.5">
    <meta name="description" content="">
    <meta name="author" content="">
-->
    <title>Jual Beli</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
	-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
    
	
<!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top navbar-custom">
        <div class="containerx">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="title">
                
				<br>&nbsp;&nbsp;&nbsp;<a href="index.php" class="title">Home</a> &nbsp;| &nbsp;
				<a href="index.php?page=order" class="title">Order</a>&nbsp;| &nbsp;
				<a href="index.php?page=item" class="title">Produk</a>
				<?php
				if($apps!="1"){
					echo "&nbsp;| &nbsp;<a href=\"logout.php\" class=\"title\">Logout</a>";
				}
				?>
				
            </div>

           
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
   </nav>
	    <!-- Header -->
    <!--  <header>
        <div class="container" id="maincontent" tabindex="-1">
            <div class="row">
                <div class="col-lg-12">
                    <img class="img-responsive" src="img/profile.png" alt="">
                   
                </div>
            </div>
        </div>
    </header> -->

	<?php
	//include("config.php");
	
	$page=$_REQUEST['page'];
	$table=$_REQUEST['table'];
	if($page==""){
		$page="default";
	}
	if($page!=""){
		
		echo "<section id=\"content\">
			<div class=\"container\">";
			
		include "pages/".$page.".php";
	
		echo "</div></section>";
		
	}else{
		include "pages/default.php";
		echo "<script src=\"vendor/jquery/jquery.min.js\"></script>";
		echo "<script src=\"vendor/bootstrap/js/bootstrap.min.js\"></script>";
	}
	
	if($apps!="1"){
		
	?>
    <!-- Footer -->
	
    <footer class="text-left">
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; MTT
                    </div>
                </div>
            </div>
        </div>
    </footer>
	<?php
	}//end not client apps
	?>
	
</body>
</html>
