<?php
session_start();
require_once('config.php');
error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));

if(isset($_POST) & !empty($_POST)){
	$username = mysqli_real_escape_string($conn, $_POST['username']);
	$password = md5($_POST['password']);

	$sql = "SELECT * FROM `user_merchant` WHERE username='$username' AND password='$password'";
	$result = mysqli_query($conn, $sql);
	$count = mysqli_num_rows($result);
	
	if($count == 1){
		 while ($row=mysqli_fetch_assoc($result))
    	 {
			session_regenerate_id();
					
			$_SESSION['username'] = $row['username'];
			$_SESSION['userid'] = $row['id'];
			session_write_close();
		 }
		
	}else{
		$fmsg = "Invalid Username/Password";
	}
}
if(isset($_SESSION['username'])){
	$smsg = "User already logged in";
	header('location: index.php');

}

$smsg=$_REQUEST['smsg'];

?>
<!DOCTYPE html>
<html>
<head>
	<title>User Login Jual Beli</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="css/bootstrap.min.css" >

	<!-- Latest compiled and minified JavaScript -->
	<script src="js/bootstrap.min.js" ></script>

	<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
      <?php if(isset($smsg)){ ?><div class="alert alert-success" role="alert"> <?php echo $smsg; ?> </div><?php } ?>
      <?php if(isset($fmsg)){ ?><div class="alert alert-danger" role="alert"> <?php echo $fmsg; ?> </div><?php } ?>
      <form class="form-signin" method="POST">
        <h2 class="form-signin-heading">Silakan Login</h2>
        <div class="input-group">
		  <span class="input-group-addon" id="basic-addon1">@</span>
		  <input type="text" name="username" class="form-control" placeholder="Username" required>
		</div>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-success btn-block" type="submit">Login</button>
        <a class="btn btn-lg btn-success btn-block" href="register.php">Register</a>
		  <a class="btn btn-lg btn-success btn-block" href="reset.php">Reset Password</a>

      </form>
</div>
</body>
</html>