<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
   <title>Terms & Policy</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <?php
if(isset($_REQUEST['type'])){
	$type=$_REQUEST['type'];
}else{
	$type="general";
}
?>
	<div class="container theme-showcase" role="main">
	  <div class="page-header">
	  <?php 
		if($type=="general"){
			echo "
			<div id=\"content\">
				<h2><div class=\"alert alert-success\">Aturan Penggunaan Jual Beli MTT</div></h2>
				<ul>
				<li>Informasi Umum</a></li>
				<li>Pengguna</li>
				<li>Merchant / Penjual Barang</li>
				<li>Transaksi</li>
				<li>Barang Terlarang</li>
				<li>Sanksi</li>
				<li>Pembatasan Tanggung Jawab</li>
				<li>Hukum yg berlaku dan penyelesaian sengkete</li>
				
			</ul>	
			</div>";
		}elseif($type=="merchant"){
			echo "<div id=\"content\">
				<h2><div class=\"alert alert-success\">Aturan Penggunaan Jual Beli MTT</div></h2>
				<ul>
				<li>Informasi Umum</a></li>
				<li>Transaksi</li>
				<li>Barang Terlarang</li>
				<li>Sanksi</li>
				<li>Pembatasan Tanggung Jawab</li>
				<li>Hukum yg berlaku dan penyelesaian sengkete</li>
			</ul>	
			</div>";
		}
		?>
		</div>
     </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>