<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Aktivasi Merchant</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<?php
if(isset($_REQUEST['msg'])){
	$msg=$_REQUEST['msg'];
}else{
	$msg="failed";
}
?>
	<div class="container theme-showcase" role="main">
	  <div class="page-header">
	  <?php 
		if($msg=="success"){
			echo "<h2><div class=\"alert alert-success\">Selamat, proses aktivasi akun sebagai merchant sudah berhasil</div></h2>";
			echo "<br>";
			echo "<p>Silakan logout dan login kembali di aplikasi untuk mendapatkan akses menu penjualan</p>";
		}else{
			echo "<h2><div class=\"alert alert-warning\">Maaf, proses aktivasi akun merchant gagal</div></h2>";
			echo "<br>";
			echo "<p>Silakan isi data dengan benar dan menggunakan email @telkomsel.co.id</p>";			
		}
		?>
		</div>
     </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>