<?php
require_once(realpath(dirname(__FILE__) . "/../tools/rest.php"));

class Product extends REST{
	
	private $mysqli = NULL;
	private $db = NULL; 
	
	public function __construct($db) {
		parent::__construct();
		$this->db = $db;
		$this->mysqli = $db->mysqli;
    }
	
	// added by emir
	public function findAllNewestProduct(){
		if($this->get_request_method() != "GET") $this->response('',406);
		$query="SELECT * FROM product p ORDER BY p.created_at DESC LIMIT 10";
		return $this->db->get_list($query);
	}


	public function findAll(){
		if($this->get_request_method() != "GET") $this->response('',406); 
		$query="SELECT * FROM product p ORDER BY p.id DESC";
		$this->show_response($this->db->get_list($query));
	}
	public function findAllProductNew(){
		if($this->get_request_method() != "GET") $this->response('',406);
		$query="SELECT * FROM product p left join product_category pc on p.id=pc.product_id WHERE pc.category_id=11 ORDER BY RAND() DESC limit 10";
		return $this->db->get_list($query);
	}

	public function findOnePlain($id){
		$query="SELECT p.*, u.id as merchant_id, u.username, u.phone FROM product p left join user_merchant u on p.merchant_id=u.id  WHERE p.id=$id LIMIT 1";
		return $this->db->get_one($query);
	}

	public function findOne(){
		if($this->get_request_method() != "GET") $this->response('',406);
		if(!isset($this->_request['id'])) $this->responseInvalidParam();
		$id = (int)$this->_request['id'];
		$this->show_response($this->findOnePlain($id));
	}

	public function allCountPlain($q, $category_id){
	    $query = "SELECT COUNT(DISTINCT p.id) FROM product p ";
	    $keywordQuery = "(p.name REGEXP '$q' OR p.status REGEXP '$q' OR p.description REGEXP '$q') ";
	    if($category_id != -1){
            $query = $query . ", product_category pc WHERE pc.product_id=p.id AND (pc.category_id=$category_id or p.category_id like '%".$category_id."%') ";
            if($q != "") $query = $query . "AND " . $keywordQuery ;
	    } else {
	        if($q != "") $query = $query . "WHERE " . $keywordQuery ;
	    }
		return $this->db->get_count($query);
	}

	public function allCountPlainForClient($q, $category_id){
	    $query = "SELECT COUNT(DISTINCT p.id) FROM product p ";
	    $keywordQuery = "(p.name REGEXP '$q' OR p.status REGEXP '$q' OR p.description REGEXP '$q') ";
	    if($category_id != -1){
            $query = $query . ", product_category pc WHERE p.draft=0 AND pc.product_id=p.id AND (pc.category_id=$category_id or p.category_id like '%".$category_id."%') ";
            if($q != "") $query = $query . "AND " . $keywordQuery ;
	    } else {
	        if($q != "") $query = $query . "WHERE p.draft=0 AND " . $keywordQuery ;
	    }
		return $this->db->get_count($query);
	}

	public function allCount(){
		if($this->get_request_method() != "GET") $this->response('',406);
		$q = (isset($this->_request['q'])) ? ($this->_request['q']) : "";
		$category_id = isset($this->_request['category_id']) ? ((int)$this->_request['category_id']) : -1;
		$this->show_response_plain($this->allCountPlain($q, $category_id));
	}

	public function findAllByPagePlain($limit, $offset, $q, $category_id){
        $query = "SELECT DISTINCT p.* FROM product p ";
        $keywordQuery = "(p.name REGEXP '$q' OR p.status REGEXP '$q' OR p.description REGEXP '$q') ";
        if($category_id != -1){
            $query = $query . ", product_category pc WHERE pc.product_id=p.id AND (pc.category_id=$category_id or p.category_id like '%".$category_id."%') ";
            if($q != "") $query = $query . "AND " . $keywordQuery ;
        } else {
            if($q != "") $query = $query . "WHERE " . $keywordQuery ;
        }
		$query = $query . "ORDER BY p.id DESC LIMIT $limit OFFSET $offset ";
		return $this->db->get_list($query);
	}

    public function findAllByPagePlainForClient($limit, $offset, $q, $category_id){
        $query = " SELECT DISTINCT p.*, m.username , m.address FROM product p LEFT JOIN user_merchant m on m.id=p.merchant_id ";
		 
		$keywordQuery = "(p.name REGEXP '$q' OR p.status REGEXP '$q' OR p.description REGEXP '$q') ";
        if($category_id != -1){
            $query = $query . ", product_category pc WHERE p.draft=0 AND pc.product_id=p.id AND (pc.category_id=$category_id or p.category_id like '%".$category_id."%') ";
            if($q != "") $query = $query . "AND " . $keywordQuery ;
        } else {
            if($q != "") $query = $query . "WHERE p.draft=0 AND " . $keywordQuery ;
        }
		///echo $query;
		
       // $query = $query . "ORDER BY p.id DESC LIMIT $limit OFFSET $offset ";
	$query = $query . "ORDER BY (FLOOR( 1 + RAND()*3)) DESC LIMIT $limit OFFSET $offset ";
		return $this->db->get_list($query);
    }

	public function findAllByPage(){
		if($this->get_request_method() != "GET") $this->response('',406);
		if(!isset($this->_request['limit']) || !isset($this->_request['page']))$this->responseInvalidParam();
		$limit = (int)$this->_request['limit'];
		$offset = ((int)$this->_request['page']) - 1;
		$q = (isset($this->_request['q'])) ? ($this->_request['q']) : "";
		$category_id = isset($this->_request['category_id']) ? ((int)$this->_request['category_id']) : -1;
		$this->show_response($this->findAllByPagePlain($limit, $offset, $q, $category_id));
	}
	
	public function insertOne(){
		if($this->get_request_method() != "POST") $this->response('',406);
		$data = json_decode(file_get_contents("php://input"),true);
		if(!isset($data) ) $this->responseInvalidParam();
		$column_names = array('name', 'image', 'type', 'category_id', 'price', 'stock', 'draft', 'description', 'status', 'created_at', 'last_update');
		$table_name = 'product';
		$pk = 'id';
		$resp = $this->db->post_one($data, $pk, $column_names, $table_name);
		$this->show_response($resp);
	}
	
	public function updateOne(){
		if($this->get_request_method() != "GET") $this->response('',406);
		//$data = json_decode(file_get_contents("php://input"),true);
		if(!isset($this->_request['id'])) $this->responseInvalidParam();
                $id = (int)$this->_request['id'];
		$data = array();
                $data['name'] = $this->_request['name'];
                $data['price'] = (int)$this->_request['price'];
                $data['stock'] = (int)$this->_request['stock'];
                $data['description'] = $this->_request['description'];
                $data['created_at'] = $this->_request['created_at'];
                $data['last_update'] = $this->_request['last_update'];
		$column_names = array('name','price','stock', 'description', 'created_at', 'last_update');
		$table_name = 'product';
		$pk = 'id';
		$query = "update product set name='".$data['name']."', price=".$data['price'].", stock=".$data['stock'].", description='".$data['description']."' where id=".$id."";
		//$this->show_response($this->db->post_update($id, $data, $pk, $column_names, $table_name));
		$this->db->update_one($query);
		$result = array("status" => "success");
		$this->show_response($result);
	}
	
	public function deleteOne(){
		if($this->get_request_method() != "GET") $this->response('',406);
		if(!isset($this->_request['id'])) $this->responseInvalidParam();
		$id = (int)$this->_request['id'];
		$table_name = 'product';
		$pk = 'id';
		$this->show_response($this->db->delete_one($id, $pk, $table_name));
	}

	public function countByDraftPlain($i){
		$query = "SELECT COUNT(DISTINCT p.id) FROM product p WHERE p.draft=$i ";
		return $this->db->get_count($query);
	}

	public function countByStatusPlain($status){
		$query = "SELECT COUNT(DISTINCT p.id) FROM product p WHERE p.status='$status' ";
		return $this->db->get_count($query);
	}
	
	
	
	////////////////////////////////////
	public function findAllByPagePlainForClientSeller($limit, $offset, $q, $merchant_id){
		
        $query = "SELECT DISTINCT p.*, m.username , m.address FROM product p LEFT JOIN 
		user_merchant m on m.id=p.merchant_id where p.merchant_id=".$merchant_id." ";
        /* $keywordQuery = "(p.name REGEXP '$q' OR p.status REGEXP '$q' OR p.description REGEXP '$q') ";
        if($merchant_id != -1){
            $query = $query . ", product_category pc WHERE p.draft=0 AND pc.product_id=p.id AND pc.category_id=$merchant_id ";
            if($q != "") $query = $query . "AND " . $keywordQuery ;
        } else {
            if($q != "") $query = $query . "WHERE p.draft=0 AND " . $keywordQuery ;
        } */
		
		//echo $query;
        //$query = $query . "ORDER BY p.id DESC LIMIT $limit OFFSET $offset ";
		$query = $query . "ORDER BY p.id DESC LIMIT $limit OFFSET $offset ";
		return $this->db->get_list($query);
    }
	public function allCountPlainForClientSeller($q, $merchant_id){
	    $query = "SELECT COUNT(DISTINCT p.id) FROM product p where merchant_id=".$merchant_id."";
		/* 
	    $keywordQuery = "(p.name REGEXP '$q' OR p.status REGEXP '$q' OR p.description REGEXP '$q') ";
	    if($category_id != -1){
            $query = $query . ", product_category pc WHERE p.draft=0 AND pc.product_id=p.id AND pc.category_id=$merchant_id ";
            if($q != "") $query = $query . "AND " . $keywordQuery ;
	    } else {
	        if($q != "") $query = $query . "WHERE p.draft=0 AND " . $keywordQuery ;
	    } */
		
		return $this->db->get_count($query);
	}
	
	public function uploadImage(){
		
		 $file_path = "/apps/www/jualbeli-webadmin/admin/uploads/product/";
		//$var = $_POST['result'];
		$file_path = $file_path . basename( $_FILES['upload']['name']);
		if(move_uploaded_file($_FILES['upload']['tmp_name'], $file_path)) {
			$result = array("status" => "success", "value" => json_encode($_FILES['upload']));
		} else{
			$result = array("status" => "failed", "value" => json_encode($_FILES['upload']));
		}
		echo json_encode($result);


	}
	
}	
?>
