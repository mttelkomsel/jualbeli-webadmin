<?php
require_once(realpath(dirname(__FILE__) . "/../tools/rest.php"));

class UserRating extends REST{
	
	private $mysqli = NULL;
	private $db = NULL; 
	
	public function __construct($db) {
		parent::__construct();
		$this->db = $db;
		$this->mysqli = $db->mysqli;
    }
	
	// added by emir
	public function findAllByProduct(){
		if($this->get_request_method() != "GET") $this->response('',406); 
		if(!isset($this->_request['product_id'])) $this->responseInvalidParam();
		$id = (int)$this->_request['product_id'];
		$query="SELECT * FROM user_rating u WHERE product_id=$id ORDER BY u.id DESC";
		$this->show_response($this->db->get_list($query));
	}	
}	
?>
