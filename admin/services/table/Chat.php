<?php
require_once(realpath(dirname(__FILE__) . "/../tools/rest.php"));

class Chat extends REST{
	
	private $mysqli = NULL;
	private $db = NULL; 
	public $conf = NULL;

	
	public function __construct($db) {
		parent::__construct();
		$this->db = $db;
		$this->mysqli = $db->mysqli;
		$this->conf = new CONF(); // Create config class
    }
	
	

	 public function _getChat($userid){
		/* $query="SELECT c.id, CASE WHEN c.from_id=5 THEN c.to_id ELSE c.from_id END AS obj_id, c.from_id, c.to_id, c.msg as content, c.created_at, u.username as title, c.status, u.phone FROM chat c LEFT JOIN user_merchant u ON c.from_id=u.id 
		WHERE (c.to_id=$userid) OR (c.from_id=$userid) GROUP BY c.to_id, c.from_id ORDER BY c.id DESC"; */
		$query="SELECT t1.*, t3.username, t3.phone,t3.email FROM chat t1 JOIN 
		 (SELECT MAX(id) AS id, from_id, to_id FROM `chat` c WHERE c.to_id=$userid GROUP BY from_id, to_id) t2
		 ON t1.from_id = t2.from_id AND t1.id = t2.id LEFT JOIN user_merchant t3 ON t1.from_id=t3.id limit 20";	
		return $this->db->get_list($query);
	}
	public function _getChatNoReply($userid, $id_array){
		$query="SELECT t1.id,t1.to_id AS from_id,t1.from_id AS to_id, t1.msg,t1.created_at,t1.status, t3.username, t3.phone,t3.email FROM chat t1 LEFT JOIN user_merchant t3 ON to_id=t3.id WHERE from_id=$userid AND to_id NOT IN(".implode(",",$id_array).") GROUP BY to_id";
		//echo $query;
		return $this->db->get_list($query);
	}

	public function getChat(){
		if($this->get_request_method() != "GET") $this->response('',406);
		if(!isset($this->_request['userid'])) $this->responseInvalidParam();
		$userid = (int)$this->_request['userid'];
		$result=$this->_getChat($userid);
		
		$id_array=array();
		for ($i = 0; $i < sizeOf($result); $i++) {
			$id_array[]=$result[$i]['id'];
		}
		//print_r($id_array);
		
		$result_noreply=$this->_getChatNoReply($userid, $id_array);
		$result_merge = array_merge($result, $result_noreply);
		
		//print_r($result_merge);
		
		//$this->show_response($this->_getChat($userid));
		$resp = array('status' => "success", "chat_user" => $result_merge);
		$this->show_response($resp);
	} 
	
	
	
	public function _getAllChatUser($userid,$from){
		$query="SELECT c.id, c.from_id, c.msg, c.created_at, u.username, u.phone FROM chat c  left join user_merchant u on c.from_id=u.id 
		WHERE (c.to_id=$userid and c.from_id=$from) or (c.to_id=$from and c.from_id=$userid) order by id asc limit 20";
		//update status=2 => read
		$query_update="UPDATE chat c set `status`=2 WHERE (c.to_id=$userid and c.from_id=$from) or (c.to_id=$from and c.from_id=$userid)";
		$res=$this->db->update_one($query_update);
		
		return $this->db->get_list($query);
	}
	
	
	public function getAllChatUser(){
		if($this->get_request_method() != "GET") $this->response('',406);
		if(!isset($this->_request['userid'])) $this->responseInvalidParam();
		$userid = (int)$this->_request['userid'];
		$from = (int)$this->_request['from'];
		//$this->show_response($this->_getAllChatUser($userid,$from));
		$result=$this->_getAllChatUser($userid,$from);
				
		$resp = array('status' => "success", "chat" => $result);
		$this->show_response($resp);
				
	}
	public function submitChat(){
		if($this->get_request_method() != "GET") $this->response('',406);
		if(!isset($this->_request['from'])) $this->responseInvalidParam();
		if(!isset($this->_request['to'])) $this->responseInvalidParam();
		if(!isset($this->_request['msg'])) $this->responseInvalidParam();
		$data['from_id'] = (int)$this->_request['from'];
		$data['to_id'] = (int)$this->_request['to'];
		$data['msg'] = $this->_request['msg'];
		$data['created_at'] = round(microtime(true) * 1000);
		
		$column_names = array('from_id', 'to_id', 'msg', 'created_at', 'status');
		$table_name = 'chat';
		$pk = 'id';
		
		//get fcmid
		$query="SELECT regid FROM fcm f WHERE f.userid=".$data['to_id'];
		$res=$this->db->get_list($query);
		$registration_ids=array();
		for($i=0;$i<sizeOf($res);$i++){
			$registration_ids[]=$res[$i]['regid'];
		}
		//print_r($registration_ids);
		//get username
		$query="SELECT username FROM user_merchant ni WHERE ni.id=".$data['from_id'];
		$arr=$this->db->get_one($query);
		/* $arr=array();
		$arr['username']=$username;
		*/
		$data['chat']=true;
	    $data['username']=$arr['username'];
		$fcm=$this->sendPushNotification($registration_ids, $data);
		
		//update DB
		$data['status']=0;
		if($fcm['success']==1)$data['status']=1;
		$resp = $this->db->post_one_plus($data, $pk, $column_names, $table_name, $arr);
		$this->show_response($resp);
				
	}
	
	public function submitMultiChat(){
		if($this->get_request_method() != "GET") $this->response('',406);
		if(!isset($this->_request['from'])) $this->responseInvalidParam();
		if(!isset($this->_request['to'])) $this->responseInvalidParam();
		if(!isset($this->_request['msg'])) $this->responseInvalidParam();
		$data['from_id'] = (int)$this->_request['from'];
		$data['to_id'] = $this->_request['to'];
		$data['msg'] = $this->_request['msg'];
		$data['created_at'] = round(microtime(true) * 1000);
		//save
		$array_user_to=$data['to_id'];
		
		
		$array_to=array();
		$array_to=explode(",",$data['to_id']);
		//print_r($array_to);
		if(sizeOf($array_to)>0){
			//check
			$data['to_id']=$array_to[0];
		}
		
		if($data['from_id']==-1){
			$data['from_id']=5; //hardcode from guest->admin
		}
		$column_names = array('from_id', 'to_id', 'msg', 'created_at', 'status');
		$table_name = 'chat';
		$pk = 'id';
		
		//get fcmid
		$query="SELECT regid FROM fcm f WHERE f.userid in (".$array_user_to.")";
		//echo $query;
		//exit();
		
		$res=$this->db->get_list($query);
		$registration_ids=array();
		for($i=0;$i<sizeOf($res);$i++){
			$registration_ids[]=$res[$i]['regid'];
		}
		//print_r($registration_ids);
		//get username
		$query="SELECT username FROM user_merchant ni WHERE ni.id=".$data['from_id'];
		$arr=$this->db->get_one($query);
		/* $arr=array();
		$arr['username']=$username;
		*/
		$data['chat']=true;
	    $data['username']=$arr['username'];
		$fcm=$this->sendPushNotification($registration_ids, $data);
		
		//update DB
		$data['status']=0;
		if($fcm['success']==1)$data['status']=1;
		$resp = $this->db->post_one_plus($data, $pk, $column_names, $table_name, $arr);
		$this->show_response($resp);
				
	}
	
	public function sendPushNotification($registration_ids, $data){
		// Set POST variables
		$url = 'https://fcm.googleapis.com/fcm/send';
		$fields = array(
			'registration_ids' => $registration_ids,
			'data' => $data
		);
		$api_key = $this->conf->FCM_KEY;
		$headers = array( 'Authorization: key='.$api_key, 'Content-Type: application/json' );
		// Open connection
		$ch = curl_init();

		// Set the url, number of POST vars, POST data
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// Disabling SSL Certificate support temporarly
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->json($fields));
		// Execute post
		$result = curl_exec($ch);
		if ($result === FALSE) { die('Curl failed: ' . curl_error($ch)); }
		// Close connection
		curl_close($ch);
		$result_data = json_decode($result,true);
		//print_r($this->json($fields));
		return $result_data;
	}
	
	/*Encode array into JSON */
	public function json($data){
		if(is_array($data)){
			return json_encode($data, JSON_NUMERIC_CHECK);
		}
	}

}	
?>